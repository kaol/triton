Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: triton
Upstream-Contact: Philippe Tillet <phil@openai.com>
Source: https://github.com/openai/triton/

Files: *
Copyright: 2018-2020 Philippe Tillet <phil@openai.com>
           2020-2024 OpenAI
License: Expat

Files: include/triton/Dialect/NVGPU/*
       include/triton/Dialect/TritonNvidiaGPU/*
       lib/Conversion/TritonGPUToLLVM/BarrierOpToLLVM.cpp
       lib/Conversion/TritonGPUToLLVM/ClusterOpsToLLVM.cpp
       lib/Dialect/NVGPU/IR/Dialect.cpp
       lib/Dialect/TritonNvidiaGPU/IR/*
       lib/Dialect/TritonNvidiaGPU/Transforms/PlanCTA.cpp
       python/test/unit/hopper/*
       python/tutorials/09-experimental-tma-matrix-multiplication.py
       python/tutorials/10-experimental-tma-store-matrix-multiplication.py
       python/tutorials/11-grouped-gemm.py
       third_party/amd/lib/TritonAMDGPUToLLVM/TensorPtrOpsToLLVM.h
       third_party/amd/lib/TritonAMDGPUTransforms/OptimizeEpilogue.cpp
       third_party/nvidia/lib/TritonNVIDIAGPUToLLVM/BarrierOpToLLVM.cpp
       third_party/nvidia/lib/TritonNVIDIAGPUToLLVM/ClusterOpsToLLVM.cpp
       third_party/nvidia/lib/TritonNVIDIAGPUToLLVM/DotOpToLLVM/WGMMA.cpp
       third_party/nvidia/lib/TritonNVIDIAGPUToLLVM/TensorPtrOpsToLLVM.cpp
       unittest/Conversion/TritonGPUToLLVM/DumpLayout.h
       unittest/Conversion/TritonGPUToLLVM/EmitIndicesTest.cpp
Copyright: 2023 NVIDIA Corporation & Affiliates
License: Expat

Files: third_party/amd/backend/include/hip/*
       third_party/amd/include/TritonAMDGPUToLLVM/GCNAsmFormat.h
       third_party/amd/lib/TritonAMDGPUToLLVM/ConvertLayoutOpToLLVM/SharedToDotOperandMFMA.cpp
       third_party/amd/lib/TritonAMDGPUToLLVM/DotOpToLLVM/MFMA.cpp
Copyright: 2015 - 2023 Advanced Micro Devices

Files: include/triton/Tools/Sys/GetEnv.hpp
       include/triton/Tools/Sys/GetPlatform.hpp
Copyright: 2015 Philippe Tillet <phil@openai.com>
License: LGPL-2.1+

Files: lib/Dialect/TritonGPU/Transforms/Pipeliner/PipelineExpander.cpp
       lib/Dialect/TritonGPU/Transforms/Pipeliner/PipelineExpander.h
Copyright: 2023-2024 OpenAI
License: APACHE-2-LLVM-EXCEPTIONS
 On Debian systems the full text of the Apache Software License 2.0 can be
 found in the `/usr/share/common-licenses/Apache-2.0' file.
 .
 ---- LLVM Exceptions to the Apache 2.0 License ----
 .
 As an exception, if, as a result of your compiling your source code, portions
 of this Software are embedded into an Object form of such source code, you
 may redistribute such embedded portions in such Object form without complying
 with the conditions of Sections 4(a), 4(b) and 4(d) of the License.
 .
 In addition, if you combine or link compiled forms of this Software with
 software that is licensed under the GPLv2 ("Combined Software") and if a
 court of competent jurisdiction determines that the patent provision (Section
 3), the indemnity provision (Section 9) or other Section of the License
 conflicts with the conditions of the GPLv2, you may retroactively and
 prospectively choose to deem waived or otherwise exclude such Section(s) of
 the License, but only in their entirety and only with respect to the Combined
 Software.

Files: debian/*
Copyright: 2023 Petter Reinholdtsen <pere@debian.org>
           2024 Kari Pahula <kaol@debian.org>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files
 (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software,
 and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: LGPL-2.1+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-2".
