#! /usr/bin/make -f

include /usr/share/dpkg/architecture.mk

export DEB_BUILD_MAINT_OPTIONS=optimize=-lto
export PYBUILD_NAME=triton
export LLVM_LIBS=$(shell llvm-config-18 --ldflags) $(shell llvm-config-18 --libs) -lMLIR -Wl,-rpath,/usr/lib/llvm-18/lib
%:
	dh $@ --with python3,sphinxdoc --buildsystem=pybuild

CONFIGURE_ARGS=\
  -DTRITON_BUILD_TUTORIALS:BOOL=OFF \
  -DTRITON_CODEGEN_BACKENDS:STRING='amd;nvidia' \
  -DCMAKE_BUILD_TYPE:STRING=RelWithDebInfo \
  -DBUILD_TESTING:BOOL=OFF \
  -DTRITON_BUILD_PYTHON_MODULE:BOOL=ON \
  -DGOOGLETEST_DIR=/usr/src/googletest/googletest \
  -DLLVM_LIBRARY_DIR=/usr/lib/llvm-18/lib

override_dh_auto_configure:
	dh_auto_configure -- --configure-args="$(CONFIGURE_ARGS)"
	pybuild --configure -d python -s distutils
	ln -sf ../../../third_party/amd/backend python/triton/backends/amd
	ln -sf ../../../third_party/nvidia/backend python/triton/backends/nvidia
# FIXME how to tell cmake to not include LLVM as both static and shared?
	for a in .pybuild/*/build/CMakeFiles/triton.dir/build.make; do \
		grep -v -E 'libLLVM.*\.a$$' $$a > debian/tmp_cmake; mv debian/tmp_cmake $$a; done
	for a in .pybuild/*/build/CMakeFiles/triton.dir/link.txt; do \
		perl -ne 's,/usr/lib/llvm-.+?/lib/libLLVM.+?.a ,,g; chomp; print "$$_ $(LLVM_LIBS)\n";' $$a > debian/tmp_cmake; mv debian/tmp_cmake $$a; done

override_dh_auto_build:
	dh_auto_build
	pybuild --install -d python -s distutils

override_dh_auto_install:
	dh_auto_install
	pybuild --install -d python -s distutils

override_dh_link:
	ls -l debian/python3-triton/usr/lib/python3/dist-packages/triton/backends/amd
	rm -r debian/python3-triton/usr/lib/python3/dist-packages/triton/backends/amd/include
	rm -r debian/python3-triton/usr/lib/python3/dist-packages/triton/backends/amd/lib
	mkdir debian/python3-triton/usr/lib/python3/dist-packages/triton/backends/amd/lib
	cd debian/python3-triton/usr/lib/python3/dist-packages/triton/backends/amd/lib; \
		for a in $(shell ls /usr/lib/*/amdgcn/bitcode); do ln -s ../../../../../../$(DEB_HOST_MULTIARCH)/amdgcn/bitcode/$$a .; done; \
		ln -s ../../../../../../$(DEB_HOST_MULTIARCH)/libamdhip64.so .
	dh_link

override_dh_fixperms:
	find debian/python3-triton/usr/lib/python3/dist-packages/ -mindepth 1 -maxdepth 1 -not -name "*triton*" -exec rm -r '{}' ';'
	mkdir debian/python3-triton/usr/lib/python3/dist-packages/triton/_C
	mv debian/python3-triton/usr/lib/python3/dist-packages/libtriton*.so debian/python3-triton/usr/lib/python3/dist-packages/triton/_C
# It's not actually functional currently despite being needed for build.
	rm -rf debian/python3-triton/usr/lib/python3/dist-packages/triton/backends/nvidia
	rm -rf debian/python3-triton/usr/lib/python3.*
	dh_fixperms

override_dh_dwz:

# --ignore-missing-info is needed because dpkg-shlibdeps will
# otherwise die at symlink /usr/lib/llvm-18/lib/libLLVM-18.so.1.
override_dh_shlibdeps:
	dh_shlibdeps -l/usr/lib/llvm-18/lib -- --ignore-missing-info

# Swinx build failed, not sure why.  Disabled until I figure out how.
skip_execute_after_dh_auto_build-indep:
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
	cd docs && \
	PYTHONPATH=$(CURDIR) http_proxy='http://127.0.0.1:9/' https_proxy='https://127.0.0.1:9/' \
	sphinx-build -N -E -T -b html . $(CURDIR)/.pybuild/docs/html/
	rm -rf $(CURDIR)/.pybuild/docs/html/.doctrees
endif
